const requestIp = require('request-ip')
const { responseSuccess, responseError, responseInvalid } = require('../middlewares/responseHandler');
const { ipSecondLastNodeToNumber } = require('../helpers/helperParkingLot');

var MAX_PARKING_SLOTS = 0;
var parkingSlots = new Array();

function createParkingLot(param) {
  MAX_PARKING_SLOTS = parseInt(param);
  for (var i = 0; i < MAX_PARKING_SLOTS
    ; i++) {
    parkingSlots.push(null);
  }

  // assign default property
  var car_number = null;
  var slot_number = null;
  for (var i = 0; i < parkingSlots.length; i++) {
    slot_number = (i + 1);
    parkingSlots[i] = { car_number, slot_number };
  }
}

function searchAvailableSlots() {
  var available = false;
  for (var i = 0; i < parkingSlots.length; i++) {
    if (parkingSlots[i].car_number == null) {
      available = true;
      break;
    }
  }
  return available;
}

async function parkCar(req, res) {
  var car_number = req.body.car_number;
  if (car_number) {
    if(parkingSlots.length == 0){
      const ipAddress = requestIp.getClientIp(req);
      createParkingLot(ipSecondLastNodeToNumber(ipAddress));
    }

    if (searchAvailableSlots()) {
      for (var i = 0; i < parkingSlots.length; i++) {
        if (parkingSlots[i].car_number == null) {
          parkingSlots[i].car_number = car_number
          return responseSuccess(res, parkingSlots[i]);
        }
      }
    } else {
      return responseSuccess(res, `Parking lot is full`);
    }
  } else {
    return responseInvalid(res, `invalid input.`);
  }
}

async function unparkCar(req, res) {
  var car_number = req.body.car_number;
  var isFound = false;
  if (car_number) {
    for (var i = 0; i < parkingSlots.length; i++) {
      if (parkingSlots[i].car_number == car_number) {
        isFound = true
        parkingSlots[i].car_number = null;
        return responseSuccess(res, {message : `slot number ${parkingSlots[i].slot_number} is free` });
      }
    }

    if (!isFound) {
      return responseSuccess(res, `car number not found.`);
    }
  } else {
    return responseInvalid(res, `invalid input.`);
  }
}

async function getSlot(req, res) {
  var value = req.body.value;
  var isFound = false;
  if (value) {
    for (var i = 0; i < parkingSlots.length; i++) {
      if (parkingSlots[i].car_number == value
        || parkingSlots[i].slot_number == value) {
        isFound = true;
        if (parkingSlots[i].car_number == null) {
          return responseSuccess(res, { message: `this slot is free.` });
        }
        return responseSuccess(res, parkingSlots[i]);
      }
    }

    if (!isFound) {
      return responseSuccess(res, `car number or slot in nof found.`);
    }
  } else {
    return responseInvalid(res, `invalid input.`);
  }
}

module.exports = {
  parkingSlots,
  createParkingLot,
  searchAvailableSlots,
  parkCar,
  unparkCar,
  getSlot
}