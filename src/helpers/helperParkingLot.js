
function ipSecondLastNodeToNumber(ip) {
  var number = ip.split('.');
  return (((+number[2])) * 256) + (+number[3]);
}

function ipToNumber(ip) {
  var number = ip.split('.');
  return ((((((+number[0]) * 256) + (+number[1])) * 256) + (+number[2])) * 256) + (+number[3]);
}

function numberToIp(number) {
  var mod = number % 256;
  for (var i = 3; i > 0; i--) {
    number = Math.floor(number / 256);
    mod = number % 256 + '.' + mod;
  }
  return mod;
}

module.exports = {
  ipSecondLastNodeToNumber,
  ipToNumber,
  numberToIp
}