const { json } = require("body-parser");

const RES_STATUS_SUCCESS = 200;
const RES_STATUS_INVALID = 400;

async function responseSuccess(res, data){
  res.status(RES_STATUS_SUCCESS);
  res.json(data);
  return res;
}

async function responseInvalid(res, message){
  res.status(RES_STATUS_INVALID);
  res.json({message: message});
  return res;
}

module.exports = {
  responseSuccess,
  responseInvalid
}