const rateLimit = require('express-rate-limit');
const MAX_REQUEST = 10;

const rateLimiter = rateLimit({
  milliSecond: 10 * 1000, // 10 second in millisecond
  max: MAX_REQUEST,
  message: `You have exceeded the ${MAX_REQUEST} requests in 10 second limit!`,
  headers: true
});

module.exports = {
  rateLimiter,
}