# node js using express

## how to setup ##
> run 'npm install' in directory parking-lot

## how to run ##
> in file ./src/configs/config.js set variable 'localIpAddress' = YOUR_LOCAL_IPV4_ADDRESS
> run 'npm start' or 'node index.js'

> Park a Car:  
hit the api park using 
url = YOUR_LOCAL_IPV4_ADDRESS/api/park
body = { "car_number": VALUE_CAR_NUMBER }

> Unpark the Car: 
hit the api park using 
url = YOUR_LOCAL_IPV4_ADDRESS/api/unpark
body = { "car_number": VALUE_CAR_NUMBER }

> Get the Car/Slot Information:  
hit the api park using 
url = YOUR_LOCAL_IPV4_ADDRESS/api/getslot
body = { "value": VALUE_CAR_NUMBER or VALUE_SLOT_NUMBER }

## how to run test ##
> run 'npm install mocha -g',
> run 'npm install chai chai-http request should --save-dev'
> run 'npm run test --recrusive --watch'

### regards ###
### fajar basoni ###
### basonifreedom@gmail.com ###