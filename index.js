const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const port = 3000;

const parkingController = require('./src/controllers/controllerParkingLot');
const { rateLimiter } = require('./src/middlewares/rateLimitRequest');
const localIpAddress = require('./src/configs/config')

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.use(rateLimiter);

app.post('/api/park/', parkingController.parkCar);
app.post('/api/unpark/', parkingController.unparkCar);
app.post('/api/getslot/', parkingController.getSlot);

const server = app.listen(port, localIpAddress, async () => {
  console.log(`Server running on port ${port}`);
});

module.exports = app;
