const assert = require('assert');
const chai = require("chai");
const chaiHttp = require("chai-http");
const expect = chai.expect;

const app = require('../../index.js');
const controllerParkingLot = require('../../src/controllers/controllerParkingLot');
const ipAddress = require('../../src/configs/config');

chai.use(chaiHttp);

describe('controllers test', () => {
  var car_number = "B 1234 NI";

  describe('createPartkingLot()', () => {
    it('should return 5', () => {
      controllerParkingLot.createParkingLot(5);
      assert.equal(controllerParkingLot.parkingSlots.length, 5);
    });
  });

  describe('searchAvailableSlots()', () => {
    it('should return true', () => {
      assert.equal(controllerParkingLot.searchAvailableSlots(), true);
    });
  });

  describe(`request POST ${ipAddress}:3000/api/park`, () => {
    it(`should return json({
        car_number: ${car_number},
        slot_number: 1
      })`, (done) => {
      chai.request(app)
        .post('/api/park/')
        .send({ "car_number": car_number })
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body.car_number).to.equal(car_number);
          done();
        });
    });
  });

  describe(`request POST ${ipAddress}:3000/api/getslot`, () => {
    it(`should return json({
      car_number: ${car_number},
      slot_number: 1
    })`, (done) => {
      chai.request(app)
        .post('/api/getslot/')
        .send({ "value": car_number })
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body.car_number).to.equal(car_number);
          expect(res.body.slot_number).to.equal(1);
          done();
        });
    });
  });

  describe(`request POST ${ipAddress}:3000/api/unpark`, () => {
    it(`should return json({
      message: "slot number 1 is free"
    })`, (done) => {
      chai.request(app)
        .post('/api/unpark/')
        .send({ "car_number": car_number })
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body.message).to.equal("slot number 1 is free");
          done();
        });
    });
  });

  describe(`request POST ${ipAddress}:3000/api/park`, () => {
    it(`should return json({
      car_number: ${car_number},
      slot_number: 1
    })`, (done) => {
      chai.request(app)
        .post('/api/park/')
        .send({ "car_number": car_number })
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body.car_number).to.equal(car_number);
          done();
        });
    });
  });

  for (var i = 0; i < 6; i++) {
    chai.request(app)
      .post('/api/getslot/')
      .send({ "value": car_number })
      .end((err, res) => {
      });
  }

  describe(`request POST ${ipAddress}:3000/api/getslot`, () => {
    it('should return status code 429', (done) => {
      chai.request(app)
        .post('/api/getslot/')
        .send({ "value": car_number })
        .end((err, res) => {
          expect(res).to.have.status(429);
          done();
        });
    });
  });
});