const assert = require('assert');
const helpers = require('../../src/helpers/helperParkingLot');
const { request } = require('express');

const ipAddress = require('../../src/configs/config');

describe('helpers test', () => {
  describe('ipSecondLastNodeToNumber()', () => {
    it('should return 11248 slots', () => {
      const number = helpers.ipSecondLastNodeToNumber(ipAddress);
      assert.equal(number, 11248);
    });
  });
  
  describe('ipToNumber()', () => {
    it('should return 3232246768', () => {
      const number = helpers.ipToNumber(ipAddress);
      assert.equal(number, 3232246768);
    });
  });

  describe('numberToIp()', () => {
    it(`should return ${ipAddress}`, () => {
      const ip = helpers.numberToIp(3232246768);
      assert.equal(ip, ipAddress);
    });
  });
});